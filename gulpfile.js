var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('scripts', function () {
  gulp.src(['./js/jquery.vmap.europe', './lib/jquery-3.3.1.js', './js/map.js'])
    .pipe(concat('mapEurope.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/'))
});

gulp.task('default', ['scripts'], function(){});