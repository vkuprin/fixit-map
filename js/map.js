(function($) {
  var maps =
    '[{"_id":"LV","country":"Latvia","companies":[{"name":"Drogas "},{"name":"Koblenz "},{"name":"Dominante Capital"},{"name":"Trialto"},{"name":"Baltā Bura "},{"name":"K-senukai "},{"name":"Spilva"},{"name":"Scania"},{"name":"Rimi Baltic"},{"name":"BENU "},{"name":"VITA mārkets "},{"name":"Apranga "},{"name":"Orkla Confectionery&Snacks "},{"name":"Latvijas Balzams"},{"name":"Kurši"},{"name":"NP Food"},{"name":"ZARA"},{"name":"Massimo Dutti"},{"name":"Konekesko "},{"name":"STORENT "},{"name":"Philip Morris Latvia SIA"},{"name":"L`Oreal "},{"name":"Reserved"},{"name":"Mohito"},{"name":"Howe"},{"name":"Cropp"},{"name":"Sinsay"}]},{"_id":"LT","country":"Lithuania","companies":[{"name":"Drogas "},{"name":"K-senukai "},{"name":"Sanitex"},{"name":"Rimi Baltic"},{"name":"BENU "},{"name":"Apranga "},{"name":"ZARA"},{"name":"Massimo Dutti"},{"name":"STORENT "},{"name":"Philip Morris Latvia SIA"},{"name":"L`Oreal "},{"name":"Ermitažas"},{"name":"Moki veži"},{"name":"Pegasas"},{"name":"TOPO centrs"},{"name":"Tele 2"},{"name":"Reserved"},{"name":"Mohito"},{"name":"Howe"},{"name":"Cropp"},{"name":"Sinsay"}]},{"_id":"EE","country":"Estonia","companies":[{"name":"K-SK-senukai "},{"name":"Rimi Baltic"},{"name":"Apranga "},{"name":"ZARA"},{"name":"Bauhof "},{"name":"Massimo Dutti"},{"name":"Telia Eesti "},{"name":"STORENT "},{"name":"Prisma Peremarket"},{"name":"LPP Estonia"},{"name":"Philip Morris Latvia SIA"},{"name":"L`Oreal "},{"name":"Reserved"},{"name":"Mohito"},{"name":"Howe"},{"name":"Cropp"},{"name":"Sinsay"}]},{"_id":"RU","country":"Russia","companies":[{"name":"K-Rauta"}]},{"_id":"UA","country":"Ukraine","companies":[{"name":"Drogas"},{"name":"Zara"}]},{"_id":"SE","country":"Sweden","companies":[{"name":"K-Rauta"}]}]';

  var map, keyFromJson, countryFromJson, companyFromJson;
  var fromServer = JSON.parse(maps);
  $(document).ready(function () {
    var BLUE = '#3d4673';
    var ORANGE = '#E27B39';
    var showCountry = '';
    var enabledCountries = ['ru', 'lv', 'ee', 'lt', 'ua', 'se'];
    var companies = $('#companies');
    var country = $('#country');

    $('#vmap').vectorMap({
      map: 'europe_en',
      enableZoom: false,
      showTooltip: true,
      multiSelectRegion: false,
      scaleColors: ['#C8EEFF', '#006491'],
      normalizeFunction: 'linear',
      selectedColor: ORANGE,
      selectedRegions: 'lv',
      hoverColor: null,
      colors: mapColors(),
      onRegionClick: function (event, code, region) {
        if (enabledCountries.indexOf(code) === -1) {
          event.preventDefault();
        }
      },
      onRegionSelect: function (event, code, region) {
        fromJson(event, code, region),
        country.html('<h1>' + countryFromJson + '</h1>');
        getCompanies(companyFromJson);
      },
      onLabelShow: function (event, label, code) {
        if (enabledCountries.indexOf(code) === -1) {
          event.preventDefault();
        }
      }
    });

    function defaultInformation(event) {
      country.html('<h1>Latvia</h1>');
      companiesJson = fromJson(event, 'LV', 'Latvia');
      getCompanies(companiesJson);
    };

    defaultInformation();

    function getCompanies(arr) {
      var html = '';
      html += '<ul class="companies-list">';

      $.each(arr, function(i) {
        $.each(arr[i], function(key, value) {
          html += ('<li>' + value + '</li>');
          companies.html(html);
        });
      });
      html += '</ul>';
    };

    function fromJson(event, code, region) {
      var countryCode = code.toUpperCase();
      $.each(fromServer, function (index, value) {
        if (value._id == countryCode) {
          countryFromJson = value.country;
          companyFromJson = value.companies;
        }
      });
      return countryFromJson && companyFromJson;
    };

    function mapColors() {
      var color = {};
      for (var i = 0; i < enabledCountries.length; i++) {
        var key = enabledCountries[i];
        color[key] = BLUE;
      }
      return color;
    };
  });
})(jQuery);